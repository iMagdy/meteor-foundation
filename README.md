Foundation for Meteor:
======================

The most advanced responsive front-end framework in the world.

This package will add the most recent version of ZURB Foundation to your Meteor.

How to use:
======================

After installing Meteorite use the following command:

<code>mrt add foundation</code>

