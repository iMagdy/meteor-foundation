Package.describe({
  summary: "The most advanced responsive front-end framework in the world."
});

Package.on_use(function (api) {
  api.use('jquery', 'client');
  api.add_files('javascripts/jquery.event.move.js', 'client');
  api.add_files('javascripts/jquery.event.swipe.js', 'client');
  api.add_files('javascripts/jquery.placeholder.js', 'client');
  api.add_files('javascripts/modernizr.foundation.js', 'client');
  
  
  api.add_files('stylesheets/foundation.min.css', 'client');
  api.add_files('stylesheets/app.css', 'client');

  api.add_files('humans.txt', 'client');
  api.add_files('robots.txt', 'client');

  api.add_files('images/foundation/orbit/bullets.jpg', 'client');
  api.add_files('images/foundation/orbit/left-arrow-small.png', 'client');
  api.add_files('images/foundation/orbit/left-arrow.png', 'client');
  api.add_files('images/foundation/orbit/loading.gif', 'client');
  api.add_files('images/foundation/orbit/mask-black.png', 'client');
  api.add_files('images/foundation/orbit/pause-black.png', 'client');
  api.add_files('images/foundation/orbit/right-arrow-small.png', 'client');
  api.add_files('images/foundation/orbit/right-arrow.png', 'client');
  api.add_files('images/foundation/orbit/rotator-black.png', 'client');
  api.add_files('images/foundation/orbit/timer-black.png', 'client');

  // XXX this makes the paths to the icon sets absolute. it needs
  // to be included _after_ the standard bootstrap css so
  // that its styles take precedence.
  api.add_files('stylesheets/bootstrap-override.css', 'client');
});